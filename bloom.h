#pragma once
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#define BFRESULTOK 0
#define BFRESULTERRNOTREADY 1

typedef int BFRESULT_T;

typedef struct {
    /* client customizable params */
    long expected_keys_cnt;     /* expected size of inserted elements */
    double expected_err;        /* expected probability of false-positive result. if not set, then 1% */
    int hashes_cnt;             /* number of hash functions. if not set, will be computed most efficient value */

    /* internal fields, clients must not use direct access to them */
    uint32_t* bitarray;
    int bitarray_len;
    int bits_cnt;
    bool ready;
} BloomFilter;

#ifdef __cplusplus
extern "C" {
#endif

BFRESULT_T bloom_filter_init(BloomFilter *f);
BFRESULT_T bloom_filter_final(BloomFilter *f);
BFRESULT_T bloom_add(BloomFilter *f, const void *key, const size_t keylen);
BFRESULT_T bloom_check(BloomFilter *f, const void *key, const size_t keylen, bool *out);

#ifdef __cplusplus
}
#endif
