#include <math.h>
#include <gmp.h>
#include "bloom.h"
#include "murmur3/murmur3.h"

const uint32_t MURMUR_SEEDS[2] = { 842140801, 2377963901 };
const int HASHDIGESTLEN = 128;

#define BFCRESULTOK 0;
#define BFCRESULTERRNOTREADY 1;

typedef int BFCRESULT_T;

#define GET_BIT_AT(N, P) ((N >> P) & 1u)
#define SET_BIT_AT(N, P, B) (N |= (B << P))

typedef struct {
    uint32_t a, b;
    bool ready;
} BloomFilterCTX;

void hash_i(const void *key, const size_t keylen, void *out, int i) {
    MurmurHash3_x64_128(key, keylen, MURMUR_SEEDS[i], out);
}

void do_bloom_add(BloomFilterCTX *ctx, BloomFilter *f);
bool do_bloom_check(BloomFilterCTX *ctx, BloomFilter *f);

BFRESULT_T bloom_filter_init(BloomFilter *f) {
    /* number of bits = -N*ln(P) / ln(2)^2 */
    if (!f->expected_err) {
        f->expected_err = .1;
    }

    f->bits_cnt = ceil(
        -((double)f->expected_keys_cnt * log(f->expected_err)) /
        0.480453013918201); // ln(2)^2

    if (!f->hashes_cnt) {
        /* number of hash functions with ignoring integrality = -log2(P) */
        f->hashes_cnt = -log2(f->expected_err);
    }

    f->bitarray_len = f->bits_cnt / 32 + 1;
    f->bitarray = (uint32_t*)malloc(f->bitarray_len * sizeof(uint32_t));
    memset(f->bitarray, 0x0u, f->bitarray_len * sizeof(uint32_t));

    f->ready = true;
#ifdef NDEBUG
    /* =====DEBUG===== */
    printf("BLOOM FILTER DEBUG:\n");
    printf("bits_cnt: %d\nhashes_cnt: %d\nbitarray_len: %d\n",
           f->bits_cnt, f->hashes_cnt, f->bitarray_len);
    printf("END OF DEBUG\n");
    /* ===END DEBUG=== */
#endif
    return BFRESULTOK;
}

BFRESULT_T bloom_filter_final(BloomFilter *f) {
    free(f->bitarray);
    f->ready = false;
    return BFRESULTOK;
}

void bloom_ctx_init(BloomFilterCTX *ctx, BloomFilter *f, const void *key, const size_t keylen) {
    uint32_t *h_a, *h_b, a, b;
    h_a = (uint32_t*)malloc(HASHDIGESTLEN);
    h_b = (uint32_t*)malloc(HASHDIGESTLEN);
    hash_i(key, keylen, h_a, 0);
    hash_i(key, keylen, h_b, 1);
    /* prepare for Kirsch-Mitzenmacher optimizied hashes generation */
    ctx->a = 32768 * h_a[0] + 1024 * h_a[1] + 32 * h_a[2] + h_a[3];
    ctx->b = 32768 * h_b[0] + 1024 * h_b[1] + 32 * h_b[2] + h_b[3];
    free(h_a);
    free(h_b);
    ctx->ready = true;
}

void bloom_ctx_final(BloomFilterCTX *ctx) {
    ctx->ready = false;
}

BFCRESULT_T set_bits(BloomFilterCTX *ctx, BloomFilter *f) {
    if (!ctx->ready) {
        return BFCRESULTERRNOTREADY;
    }
    int position, block_i;
    for (int i = 0; i < f->hashes_cnt; i++) {
        position = (ctx->a + i * ctx->b) % f->bits_cnt;
        block_i = position / f->bitarray_len;
        position %= f->bitarray_len;
        SET_BIT_AT(f->bitarray[block_i], position, 1);
    }
    return BFCRESULTOK;
}

BFCRESULT_T test_bits(BloomFilterCTX *ctx, BloomFilter *f, bool *out) {
    if (!ctx->ready) {
        return BFCRESULTERRNOTREADY;
    }
    int position, block_i;
    for (int i = 0; i < f->hashes_cnt; i++) {
        position = (ctx->a + i * ctx->b) % f->bits_cnt;
        block_i = position / f->bitarray_len;
        position %= f->bitarray_len;

        if (!GET_BIT_AT(f->bitarray[block_i], position)) {
            *out = false;
            return BFCRESULTOK;
        }
    }
    *out = true;
    return BFCRESULTOK;
}

BFRESULT_T bloom_add(BloomFilter *f, const void *key, const size_t keylen) {
    if (!f->ready) {
        return BFRESULTERRNOTREADY;
    }

    BloomFilterCTX ctx;
    bloom_ctx_init(&ctx, f, key, keylen);
    set_bits(&ctx, f);
    bloom_ctx_final(&ctx);
    return BFRESULTOK;
}

BFRESULT_T bloom_check(BloomFilter *f, const void *key, const size_t keylen, bool *out) {
    if (!f->ready) {
        return BFRESULTERRNOTREADY;
    }

    BloomFilterCTX ctx;
    bloom_ctx_init(&ctx, f, key, keylen);
    test_bits(&ctx, f, out);
    bloom_ctx_final(&ctx);
    return BFRESULTOK;
}

int main(int argc, char *argv[]) {
    BloomFilter filter = {
        .expected_keys_cnt = 1000,
        .expected_err = .01,
    };
    bloom_filter_init(&filter);

    const char *word1 = "word1",
        *word2 = "word2",
        *word3 = "hello",
        *word4 = "bye",
        *word5 = "qwerty";

    bloom_add(&filter, word1, strlen(word1));
    bloom_add(&filter, word2, strlen(word2));
    /* bloom_add(&filter, word3, strlen(word3)); */
    bloom_add(&filter, word4, strlen(word4));
    /* bloom_add(&filter, word5, strlen(word5)); */

    bool flag;

    bloom_check(&filter, word1, strlen(word1), &flag);
    printf("check word1: %d\n", flag);
    bloom_check(&filter, word2, strlen(word2), &flag);
    printf("check word2: %d\n", flag);
    bloom_check(&filter, word3, strlen(word3), &flag);
    printf("check word3: %d\n", flag);
    bloom_check(&filter, word4, strlen(word4), &flag);
    printf("check word4: %d\n", flag);
    bloom_check(&filter, word5, strlen(word5), &flag);
    printf("check word5: %d\n", flag);

    bloom_filter_final(&filter);
}
